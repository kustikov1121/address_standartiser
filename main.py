#removing russia, index and len obl

import pandas as pd
import json


excel = 'analyse.xlsx'
#pretty_excel = 'edited_file.xlsx'
#pretty_df = pd.read_excel(pretty_excel).values.tolist()


names_dict = {}


names_of_objects = ['аллея', 'б-р', 'дорога',
                    'дор', 'линия', 'наб',
                    'п', 'пер', 'пл', 'площадь',
                    'пр', 'пр-кт', 'проезд', 'ул', 'улица',
                    'ш', 'шоссе', 'проспект', "набережная",
                    'ленинградская']

dict_with_name_types = {'аллея': ['аллея'],
                        'бульвар': ['б-р'],
                        'дорога' : ['дор', 'дорога'],
                        'линия': ['лин', 'линия'],
                        'набережная': ['набережная', 'наб', 'наб.'],
                        'проспект': ['пр', 'пр-кт', 'проспект', 'п'],
                        'переулок': ['пер', 'переулок'],
                        'площадь': ['пл', 'площадь'],
                        'проезд': ['проезд'],
                        'улица': ['ул', 'улица'],
                        'шоссе': ['ш', 'шоссе']}

dom = ['д', 'дом']


df = pd.read_excel(excel).values.tolist()


def make_your_style(val):
    color = 'white'
    if 'ленинградская' in str(val).lower():
        color = 'red'
    #if color == 'red':
        #print (val)

    return f'background-color: {color}'


#print (df)

remove = ['Россия, Санкт-Петербург, Санкт-Петербург, ',
          'Россия, г Санкт-Петербург, ',
          'Россия, Санкт-Петербург г, ',
          'Россия, Санкт-Петербург, ',
          'Россия,г Санкт-Петербург,,,,',
          'Санкт-Петербург, ']

#новый список для для уникальных значений
"""unique = []
for n in df:

    except:
        pass
    if n not in unique:
        #adding to UNIQUE list
        print (n)
        unique.append(n)
"""


def index_remover():
    for position, n in enumerate(df):
        try:
            # удаляем индексы
            #print (int(n[0][:6]))
            int(n[0][:6])
            df[position] = [n[0][8:]]
            #print (n)

            #print('index deleted')

        except:
            pass
    return df


def remove_russia(dataframe):
    cnt = 0

    for position, n in enumerate(dataframe):


        #CRemoving Russia, and Saint P
        t=str(n[0]).lower()
        for variant in remove:
            if variant.lower() in t:
                n[0] = n[0].replace(variant, '')



        n[0] = n[0].split(';')[0].replace(',дом', ', д. ')
        # count rows
        one_time_per_row = True

        splitted = n[0].split(' ')
        for m in splitted:
            m_cleared = m.replace(',', '').replace('.', '')
            if m_cleared.lower() in names_of_objects and one_time_per_row == True:
                one_time_per_row = False
                cnt += 1


    #print(cnt)

    return dataframe


def extract_UL(dataf):
    list_of_objects = []
    for p, name in enumerate(dataf):
        object_dict = {'type_of_obj': None,
                       'street_name': None,
                       'number': None,
                       'additional': None}
        split_useless = name[0].split(',')[0]
        dom = name[0].split(',')[1]
        additional = name[0].split(',')[2:]
        t=0
        standartize_name = split_useless.split(' ')
        for n in standartize_name:

            for key in dict_with_name_types:

                for value in dict_with_name_types[key]:
                    #print (standartize_name)


                    if n.lower() == value.lower():
                        object_dict['type_of_obj'] = key
                        standartize_name.remove(n)
                        #print (p)
                        #print (key)
                        t=1
        text = ''
        for j in standartize_name:
            text +=f'{j} '
        object_dict['street_name'] = text
        object_dict['number'] = dom
        additional_text = ''
        for k in additional:
            additional_text +=k
        object_dict['additional'] = additional_text
        list_of_objects.append(object_dict)
#            dataf[p] = [f'{key} {text}', dom]
        t=1
#            if t=1:
#            try:
#                dataf.remove(name)
#            except:
#                pass
#                        else:
#                            print ('FAILED')
#    print ((json.dumps(list_of_objects, ensure_ascii=False, indent=4).encode('utf8')).decode())
#    print (len(list_of_objects))
    return list_of_objects




        






#print (len(df))
data_without_rus = remove_russia(index_remover())

extractedUL = extract_UL(data_without_rus)

#panded_data = pd.DataFrame(data_without_rus)
#panded_data.style.applymap(make_your_style).to_excel('edited_file.xlsx', encoding='utf-8', index=False, header=False)
panded_data = pd.DataFrame(extractedUL)
panded_data.style.applymap(make_your_style).to_excel('edited_file.xlsx', encoding='utf-8', index=False, header=False)
#panded_data.to_excel('edited_file.xlsx', encoding='utf-8', index=False, header=False)



#print (list(names_dict.keys()))
#print(sorted(names_of_objects))







